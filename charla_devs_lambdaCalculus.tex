\documentclass{beamer}

\usepackage[utf8]{inputenc}
\usepackage{default}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{multicol}
\setlength{\columnsep}{1cm}
\usepackage{xcolor}
\definecolor{ao}{rgb}{0.0, 0.5, 0.0}

\usetheme{Dresden}
\title[Teoría de la computación]{Cálculo-$\lambda$:}
\subtitle{una formalización de los lenguajes de programación}
\author{Moisés Vázquez}
\institute{RMCorp}
\date{3 de junio del 2022}

\begin{document}

\logo{\includegraphics[height=1.6cm]{img/buk_logo.png}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \titlepage
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{}
  \framesubtitle{}
  \large
  \centering
  \begin{minipage}{9cm}
    {\em Never had any mathematical conversations with anybody,
      because there was nobody else in my field.}\\ ---Alonzo Church

    \vspace{2cm}

    {\em The $\lambda$-calculus can be called the smallest universal programming
      language in the world.}\\ ---Raul Rojas
  \end{minipage}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{¿Cuál es la intención de esta charla?}
  \framesubtitle{Tratar de responder las siguientes preguntas}
  \large
  \centering

  \pause
  ¿Qué es el cálculo-$\lambda$?

  \vspace{0.5cm}
  \pause
  ¿Cómo se programa en el cálculo-$\lambda$?

  \vspace{0.5cm}
  \pause
  ¿Cuánto poder de expresividad tiene el cálculo-$\lambda$?

  \vspace{0.5cm}
  \pause
  ¿Cómo impacta el cálculo-$\lambda$ en la industria?

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{Introducción:}
  \framesubtitle{}
  \small
  \parbox{0cm}{\includegraphics[height=5cm]{img/Alonzo_Church.jpg}} \hfill
  \parbox{7cm}{\begin{itemize}\pause
      \item El cálculo-$\lambda$ es un sistema formal creado por Alonzo Church, lo publicó
            en 1932.\pause
      \item Es un {\em lenguaje de funciones}.\pause
      \item Fue pensado inicialmente como un fundamento {\em funcional} de las matemáticas. Pero
            en 1935 Kleene y Rosser probaron que era un sistema inconsistente.\pause
      \item En 1960, fue re-descubierto como una herramienta versátil en ciencias de la computación
            por McCarthy, Strachey, Landin, y Scott.
    \end{itemize}
  }
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{$\lambda^U$}
  \framesubtitle{}
  \pause
  $$e ::=\ x\ |\ \lambda x. e\ |\ e\ e$$

  \pause
  \begin{itemize}
    \item El esquema para denotar variables. En adelante, letras minúsculas.
    \item Funciones anónimas cuyo argumento es $x$ y cuyo cuerpo es $e$.
    \item Denota el aplicar una función a un término.
  \end{itemize}

  \pause
  {\bf No pueden obviarse paréntesis.}

  Por convención, la expresión $xyz$ se interpreta como $(xy)z$.
  \pause

  Algunos ejemplos:
  \begin{center}
    $\lambda z.z$

    $\lambda x.\lambda y. x(yz)$

    $z\ ((\lambda y.yz)\ x)$
  \end{center}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{$\lambda^U$}
  Una variable está {\em acotada} si hay una lambda que la cuantifique.

  Una variable está {\em libre} si no hay una lambda que la cuantifique.

  \pause\centering \vspace{1cm}

  $\lambda z.z$ --- La variable $z$ está acotada.

  $\lambda x.\lambda y. x(yz)$ --- $x$ está acotada, $y$ está acotada, $z$ está libre

  $z\ ((\lambda y.yz)\ x)$ --- $y$ está acotada, $z$ está libre, $x$ está libre

  \pause\vspace{1cm}

  $FV(e)$ denota todas las variables libres de $e$.

  \pause

  Un {\em programa} es un término sin variables libres.
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{$\lambda^U$}
  Regla de reducción:

  $$ (\lambda x.t)\ s \rightarrow_{\beta} t[x:=s]$$

  \pause donde:
  \begin{itemize}
    \item $x[x:=r] = r$
    \item $y[x:=r] = y$ \hspace{0.5cm} si $x \neq y$
    \item $(ts)[x:=r] = (t[x:=r])(s[x:=r])$
    \item $(\lambda z.t)[x:=r] = \lambda z.t[x:=r]$ \hspace{0.5cm} si $z \neq x$, $z\in FV(r)$
  \end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{$\lambda^U$}
  Ejemplos:

  \begin{itemize}
    \item $(\lambda x.x(xy))r \rightarrow_{\beta} r(ry)$
    \item $(\lambda x.y)r \rightarrow_{\beta} y$
    \item $(\lambda x.(\lambda y.yx)z)v \rightarrow_{\beta} (\lambda y.yv)z$
    \item $(\lambda y.yv)z \rightarrow_{\beta} zv$
  \end{itemize}

  \vspace{0.3cm}

  \pause
  Un término de la forma ``$(\lambda x.t)\ r$'' se llama redex.

  \pause
  Decimos que $t \rightarrow_{\beta}^\star t'$ si $t$ se reduce en una cantidad finita de reducciones a $t'$.

  Si un término no puede reducirse más, decimos que está en {\em forma normal}.

  \pause
  Sean $\omega = \lambda x. xx$, $\Omega = \omega \omega$, entonces:

  $$\Omega \rightarrow_{\beta} \Omega \rightarrow_{\beta} \Omega \hdots $$

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{$\lambda^U$}
  El término $(\lambda x.(\lambda y.yx)z)v$ tiene dos redex. ¿Importa por cuál redex se empiece?

  \vspace{0.5cm}

  \pause
  {\bf Teorema} ({\em Confluencia}) (Church-Rosser) Si $t \rightarrow_{\beta} r$ y $t \rightarrow_{\beta} s$, entonces
  existe un término $t'$ tal que $r \rightarrow_{\beta} t'$ y $s \rightarrow_{\beta} t'$.

  \vspace{0.5cm}

  \pause
  {\bf Teorema} Si un término tiene forma normal, es única.

  \pause\vspace{0.5cm}

  Evaluación perezosa:

  $$ ((\lambda x. \lambda y . x z)\ (\lambda v. v w))\ (\text{\scshape Lo-que-sea}) \rightarrow_{\beta}^\star$$
  $$ (\lambda y . (\lambda v. v w) z )\ (\text{\scshape Lo-que-sea}) \rightarrow_{\beta}^\star$$
  $$ (\lambda v. v w) z \rightarrow_{\beta}^\star zw$$
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{$\lambda^U$}
  \centering

  \includegraphics[height=6cm]{img/what.png}
  \Large

  ¿Y aquí cómo se programa?
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{$\lambda^U$}
  Booleanos:

  \begin{itemize}
    \item {\scshape True} := $\lambda x.\lambda y.x$
    \item {\scshape False} := $\lambda x.\lambda y.y$
    \item {\scshape ift} := $\lambda b.\lambda t.\lambda e. (bt)e$
    \item {\scshape Not} := $\lambda z.(z$ {\scshape False}) {\scshape True}
    \item {\scshape And} := $\lambda x.\lambda y. (x y) x$
  \end{itemize}

  \pause\vspace{0.5cm}
  Pares ordenados $(x, y)$:

  \begin{itemize}
    \item {\scshape Pair} := $\lambda f.\lambda s.\lambda p. (pf)s$
    \item {\scshape Fst} := $\lambda p.p$ {\scshape True}
    \item {\scshape Snd} := $\lambda p.p$ {\scshape False}
  \end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{$\lambda^U$}
  \small
  Los numerales de Church se definen como:
  \begin{description}
    \item 0 := $\lambda s.\lambda z.z$
    \item 1 := $\lambda s.\lambda z.s\ z$
    \item 2 := $\lambda s.\lambda z.s\ (s\ z)$
    \item 3 := $\lambda s.\lambda z.s\ (s\ (s\ z))$\\
          $\vdots$
  \end{description}

  \pause

  Idea intuitiva: Hacer $n+m$ es aplicar sucesor $n$ veces a $m$.

  \pause
  \begin{itemize}
    \item {\scshape Suc} := $\lambda n.\lambda s.\lambda z.s((ns)z)$
    \item {\scshape Sum} := $\lambda n.\lambda m.(n\ \text{\scshape Suc})m$
    \item {\scshape Prod} := $\lambda n.\lambda m.(n(\text{\scshape Suma } m))0$
    \item {\scshape IsZero} := $\lambda n.n(\lambda x.$ {\scshape False}$)$ {\scshape True}
  \end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{$\lambda^U$}
  \framesubtitle{}
  \centering

  \includegraphics[height=5.7cm]{img/wtf.jpg}

  ¿Y la diversión?
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{$\lambda^U$}
  \framesubtitle{Recursión}
  Un término $F$ es un {\em combinador de punto fijo} si y sólo si cumple:

  $$Fg \rightarrow_{\beta}^\star g(Fg)$$

  \pause
  Por ejemplo, podemos definir al factorial como:

  $$ \text{\scshape Fac} := Fg$$

  donde:

  \centering

  \scshape $g = \lambda f.\lambda n.$(ift ((IsZero $n$) 1)) ((Mult $n)\ (f$ (Pred $n$)))
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{$\lambda^U$}
  \framesubtitle{Recursión}
  \small
  Ejemplos de combinadores de punto fijo:

  \begin{itemize}
    \item Curry-Roser:

          $Y = \lambda f.(\lambda x.f(xx))(\lambda x.f(xx))$

    \item Turing:

          $V = UU$, donde $U = \lambda f\lambda x.x(ffx)$

    \item Klop:

          $K = LLL\hdots L$ (26 veces), donde

  \end{itemize}
  $L = \lambda abcdefghijklmnopqstuvwxyzr.r(thisisafixedpointcombinator)$

  \pause\vspace{0.7cm}
  {\bf Tesis Church-Turing}: Las máquinas de Turing y $\lambda^U$ son equivalentes. \pause
  Los algoritmos son máquinas de Turing o términos del cálculo-$\lambda$.
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{$\lambda^U$}
  \framesubtitle{}
  \footnotesize

  \begin{center}
    \includegraphics[height=4cm]{img/RipperRooTwinsanity.jpg}
  \end{center}

  LOS TÉRMINOS PUEDEN SER FUNCIONES, BOOLEANOS Y NÚMEROS NATURALES AL MISMO TIEMPO

  SON EQUIVALENTES:

  $\lambda n f x. n (\lambda g h. h (g f)) (\lambda  u. x) (\lambda u. u)$

  \hspace{1cm} $\lambda n. n (\lambda g k. (g 1) (\lambda u.${\scshape Sum }$ (g k) 1) k) (\lambda v. 0) 0$

  AMBOS CALCULAN {\scshape Pred} \pause

  SABER SI DOS TÉRMINOS SON EQUIVALENTES ES INDECIDIBLE
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{}
  \framesubtitle{}
  \footnotesize
  \centering
  \Large
  \includegraphics[height=7cm]{img/elegant.jpg}
  \scshape Cálculo-$\lambda$ con tipos
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{El cubo de Barendregt}
  \framesubtitle{}
  \footnotesize
  \centering
  \Large
  \includegraphics[height=7.1cm]{img/cubo.png}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{$\lambda^{\rightarrow}$}
  \framesubtitle{Cálculo-$\lambda$ con tipos simples [1940]}
  \small

  Sintaxis:
  \begin{center}
    \includegraphics[height=0.5cm]{img/syntax_simple.png}\\
    \includegraphics[height=0.5cm]{img/types_simple.png}
  \end{center}

  Reglas de tipado:

  \begin{center}
    \includegraphics[height=2.5cm]{img/rules_simple.png}
  \end{center}

  \begin{itemize}
    \item Menos expresivo que $\lambda^U$.
    \item No hay una función identidad genérica.
    \item Se puede tener inferencia de tipos (algoritmo W).
  \end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{$\lambda 2$}
  \framesubtitle{}
  \small

  \begin{itemize}
    \item \pause Descubierto por Jean-Yves Girard (1972) y por John C. Reynolds (1974) de manera independiente.
    \item \pause Introduce la sintaxis para tipos $\forall \alpha . \tau$
    \item \pause {\bf Polimorfismo}: $\lambda x^{\alpha}. x : \forall \alpha . \alpha \rightarrow \alpha$
    \item \pause La inferencia de tipos en $\lambda 2$ es indecidible.
    \item \pause Hay una restricción de las reglas para $\lambda 2$, {\em Hindley–Milner}, que permite que la inferencia de tipos se vuelva computable.
    \item \pause $\lambda 2$ junto con la restricción {\em Hindley–Milner} son la base téorica de:
  \end{itemize}
  \centering
  \includegraphics[height=2cm]{img/logoH.png}

  \pause {\bf Siguiente charla}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}{$\lambda P_{\omega}$}
  \begin{itemize}
    \item \pause El cálculo-$\lambda$ más expresivo y poderoso.
    \item \pause También conocido como el {\em cálculo de construcciones.}
    \item \pause Además de ser un lenguaje de programación, sirve como fundamento para las matemáticas.
    \item \pause Es la base teórica del asistente de pruebas {\scshape Coq}.
  \end{itemize}
  \centering \pause

  \includegraphics[height=4cm]{img/rana.png}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{$\lambda P_{\omega}$}
  \framesubtitle{Vladimir Voevodsky (1966 - 2017) Medalla Fields 2002}
  \centering
  \includegraphics[height=6.6cm]{img/vlad1.jpg}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{$\lambda P_{\omega}$}
  \framesubtitle{Vladimir Voevodsky (1966 - 2017) Medalla Fields 2002}
  \centering
  \includegraphics[height=6.6cm]{img/vlad2.jpg}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{$\lambda P_{\omega}$}
  \framesubtitle{Vladimir Voevodsky (1966 - 2017) Medalla Fields 2002}
  \centering
  \includegraphics[height=6cm]{img/vladimir.png}

  \pause

  {\em Calculus of constructions $\sim$ CoC $\sim$ Coq}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{$\lambda P_{\omega}$}
  \framesubtitle{Tree sort}
  \begin{center}
    \includegraphics[height=4cm]{img/bst.png}
  \end{center}

  Dada una lista de números naturales:
  \begin{itemize}
    \item Convierte la lista en un árbol de búsqueda binaria.
    \item Haz un recorrido {\em in-order} (l - n - r) en el árbol y devuelve el orden de ese recorrido.
  \end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{$\lambda P_{\omega}$}
  \framesubtitle{Coq}
  \begin{itemize}
    \item Isomorfismo de Curry-Howard:
          \begin{center}
            programas funcionales tipados $\simeq$ demostraciones matemáticas
          \end{center}
    \item Es la propuesta para cambiar la forma de hacer matemáticas y desarrollo de software.
    \item Ya hay una inmensa cantidad de teoremas formalizados.
    \item Aplicaciones en la industria: París verificó formalmente sus sistemas de control de trenes.
    \item Enseñanza de las matemáticas.
    \item \pause {\em ¿Quién verifica al asistente?} \pause El núcleo de Coq es lo
          suficientemente pequeño para ser revisado por humanos.
    \item \pause {\em Verificar puede ser tardado y doloroso.}

          ---Jeremy Avigad
  \end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{}
  \framesubtitle{}
  \includegraphics[height=6.3cm]{img/funcs.png}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{¿Por qué la programación funcional?}
  \framesubtitle{}
  \centering
  \includegraphics[height=6.3cm]{img/wiss.png}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{}
  \framesubtitle{}
  \LARGE
  \begin{center}
    {\em El futuro de la humanidad es funcional}

    \pause \vspace{2cm}

    \Huge
    Gracias
  \end{center}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}
